const { Command } = require("commander");
const program = new Command();
const { listObjectsInBucket } = require("./commands/list");
const { uploadFile } = require("./commands/upload");
const {deleteFile} = require("./commands/delete");

program.name("super S3 manager");

program
  .command("list")
  .description("list files in bucket")
  .requiredOption("-b, --bucket <bucket>", "bucket name")
  .option("-p, --pattern <pattern>", "pattern to filter the list")
  .action(async (options) => {
    const { bucket: bucketName, pattern } = options;
    const files = await listObjectsInBucket(bucketName, pattern);
    if (files.length === 0) {
      console.log('no files');
    }
    files.map((name) => console.log(name));
  });

program
  .command("upload")
  .description("upload file from local drive")
  .requiredOption("-b, --bucket <bucket>", "bucket name")
  .requiredOption("-f, --file <filePath>", "path and name of the file")
  .requiredOption(
    "-k, --key <keyName>",
    "name which will be set to file inside bucket"
  )
  .action((options) => {
    const { bucket: bucketName, file: filePath, key: keyName } = options;
    uploadFile(filePath, bucketName, keyName);
  });

program
  .command("delete")
  .description("delete files matching pattern")
  .requiredOption("-b, --bucket <bucket>", "bucket name")
  .requiredOption("-p, --pattern <pattern>", "pattern to filter the list")
  .action(async (options) => {
    const { bucket: bucketName, pattern } = options;
    const files = await listObjectsInBucket(bucketName, pattern);
    if(files.length > 0) {
      files.map( fileName => deleteFile(bucketName, fileName))
    }
    else {
      console.log('no files to delete')
    }
  });

program.parse();
