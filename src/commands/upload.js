const { s3 } = require("../auth");
const fs = require("fs");

const uploadFile = async (filePath, bucketName, keyName) => {
  // Read the file
  const file = fs.readFileSync(filePath);

  // Setting up S3 upload parameters
  const uploadParams = {
    Bucket: bucketName, // Bucket into which you want to upload file
    Key: `x-wing/${keyName}`, // Name by which you want to save it
    Body: file, // Local file
  };

  try {
    const data = await s3.upload(uploadParams).promise();
    console.log("Upload Success", data.Location);
  } catch (err) {
    console.log("Error", err);
  }
};

exports.uploadFile = uploadFile;
