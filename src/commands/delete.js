const { s3 } = require('../auth');

const deleteFile = async (bucketName, fileName) => {
    const params = {
        Bucket: bucketName,
        Key: fileName
    };

    try {
        const data = await s3.deleteObject(params).promise();
        console.log(`File deleted successfully: ${fileName}`);
    } catch (error) {
        console.error(`Error deleting file: ${fileName}`, error);
    }
};

exports.deleteFile = deleteFile;
