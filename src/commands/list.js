const { s3 } = require('../auth');

const listObjectsInBucket = async (bucketName, pattern = '') => {
    // Create the parameters for calling listObjects
    var bucketParams = {
        Bucket: bucketName,
        Prefix: 'x-wing' 
    };
  
    // Call S3 to obtain a list of the objects in the bucket
    try {
        const response = await s3.listObjects(bucketParams).promise();
        let files = response.Contents.map( item => item.Key);
        if (pattern.length > 0) {
            const filter = new RegExp(pattern)
            files = files.filter( name => filter.test(name))
        }
        return files;
    } catch (error) {
        console.error(error);
    }
}

exports.listObjectsInBucket = listObjectsInBucket;
