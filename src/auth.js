const AWS = require('aws-sdk');

// Configure the AWS SDK
AWS.config.update({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
});

// Create an S3 service object
const s3 = new AWS.S3();

exports.s3 = s3;
